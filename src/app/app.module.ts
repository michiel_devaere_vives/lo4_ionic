import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { GalleryPage } from '../pages/Gallery/Gallery';
import { UploadPage } from '../pages/Upload/Upload';
import { GyroPage } from '../pages/Gyro/Gyro';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import{ HttpClientModule} from '@angular/common/http';

import { Camera } from '@ionic-native/camera';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { DeviceMotion } from '@ionic-native/device-motion';
import { NetworkEngineProvider } from '../providers/network-engine/network-engine';

@NgModule({
  declarations: [
    MyApp,
    GalleryPage,
    UploadPage,
    GyroPage,
    TabsPage
  ],
  imports: [
    BrowserModule, HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    GalleryPage,
    UploadPage,
    GyroPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen, DeviceMotion,
    {provide: ErrorHandler, useClass: IonicErrorHandler}, FingerprintAIO, Camera,
    NetworkEngineProvider
  ]
})
export class AppModule {}
