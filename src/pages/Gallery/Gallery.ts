import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NetworkEngineProvider } from '../../providers/network-engine/network-engine';


@Component({
  selector: 'page-Gallery',
  templateUrl: 'Gallery.html'
})
export class GalleryPage {
  converted_image : any;
  responseTxt: any;
  StringSplit : any;

  public photos : any;
  constructor(public network: NetworkEngineProvider) {
  }

  ionViewDidEnter() {
    this.photos = [];
    this.network.readTable().then(data => {
      this.StringSplit = data.split('@');
      for (let index = 0; index < this.StringSplit.length-1; index= index+2) {
        this.converted_image= "data:image/jpeg;base64,"+ this.StringSplit[index];
          this.photos.push(this.StringSplit[index+1]);
      }
      console.log(this.photos);
    }, (err) => {
      console.log(err);
    })
  }
}
