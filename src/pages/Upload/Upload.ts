import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NetworkEngineProvider } from '../../providers/network-engine/network-engine';

@Component({
  selector: 'page-Upload',
  templateUrl: 'Upload.html'
})
export class UploadPage {

  public photos: any;
  public base64Image: string;
  public ImageData: string;

  x: string;
  constructor(private camera: Camera, public network: NetworkEngineProvider) {

  }
  ngOnInit() {
    this.photos = [];
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      targetHeight : 300,
      targetWidth : 300,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.photos.push(this.base64Image);
      this.photos.reverse();
    }, (err) => {
      console.log(err);
    });
  }

  addPhoto(img) {
    this.network.writeTable(img).then(data => {
      alert("Image Added");
    })
      .catch(error => {
        console.error(error);

      })
  }

  deletePhoto(index){
    this.photos.splice(index, 1);
  }
}
