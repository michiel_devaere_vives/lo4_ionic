import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DeviceMotion, DeviceMotionAccelerationData, DeviceMotionAccelerometerOptions } from '@ionic-native/device-motion';
import * as THREE from 'three';

@Component({
  selector: 'page-Gyro',
  templateUrl: 'Gyro.html'
})
export class GyroPage {


  @ViewChild('domObj') canvasEl: ElementRef;

  x: string;
  y: string;

  id: any;

  private _ELEMENT: any;
  private _SCENE;
  private _CAMERA;
  public renderer;
  private _GEOMETRY;
  public _MATERIAL;
  public _CUBE;




  constructor(public deviceMotion: DeviceMotion) {
    this.x = "-";
    this.y = "-";

    try {
      var option: DeviceMotionAccelerometerOptions =
      {
        frequency: 100
      };

        this.id = this.deviceMotion.watchAcceleration(option).subscribe((acc: DeviceMotionAccelerationData) => {
        this.x = "" + acc.x;
        this.y = "" + acc.y;

        this._CUBE.rotation.x += acc.y/100;
        this._CUBE.rotation.y += -acc.x/100;

      });


    }
    catch (error) {
      alert("error" + error);

    }
  }

  ionViewDidLoad(): void {
    this.initialiseWebGLObjectAndEnvironment();
    this.renderAnimation();
  }

  initialiseWebGLObjectAndEnvironment(): void {
    this._ELEMENT = this.canvasEl.nativeElement;

    this._SCENE = new THREE.Scene();

    this._CAMERA = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    this._CAMERA.position.z = 5;

    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(window.innerWidth/1.5 , window.innerHeight/1.5 );
    this._ELEMENT.appendChild(this.renderer.domElement);

    this._GEOMETRY = new THREE.BoxGeometry(2, 2, 2);
    this._MATERIAL = new THREE.MeshBasicMaterial({ color: 0xFF0000, wireframe: true});
    this._CUBE = new THREE.Mesh(this._GEOMETRY, this._MATERIAL);

    this._SCENE.add(this._CUBE);
  }

  private _animate(): void {
    requestAnimationFrame(() => {
      this._animate();
    });
    this.renderer.render(this._SCENE, this._CAMERA);
  };

  renderAnimation(): void {
    this._animate();
  }
}
