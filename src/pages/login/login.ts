import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, private faio: FingerprintAIO) {
  }

  login() {
    this.faio.show({
        clientId: 'Fingerprint-demo',
        clientSecret: "password"
    })
    .then(result => {
      this.navCtrl.setRoot(TabsPage);
    })
    .catch(err => {
      console.log(err);
    })
  }

}
