import { Component } from '@angular/core';

import { GalleryPage } from '../Gallery/Gallery';
import { UploadPage } from '../Upload/Upload';
import { GyroPage } from '../Gyro/Gyro';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = GyroPage;
  tab2Root = UploadPage;
  tab3Root = GalleryPage;

  constructor() {

  }
}
