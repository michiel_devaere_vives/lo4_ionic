import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the NetworkEngineProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NetworkEngineProvider {

  constructor(public http: HttpClient) {
  }

  readTable() : Promise<any>{
    let url = "http://81.245.159.9/api/read.php";
    let request = this.http.get(url);

    return request.toPromise();
  }

  writeTable(img) : Promise<any>{
    let url = "http://81.245.159.9/api/create.php";

    let param = {img: img};

    let request = this.http.post(url, param);

    return request.toPromise();
  }

}
